/**
 * 
 */
package org.cloudbus.cloudsim.gpu.allocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.gpu.Vgpu;
import org.cloudbus.cloudsim.gpu.VideoCard;

/**
 * {@link VideoCardAllocationPolicy} is an abstract class that represents the
 * provisioning of video cards to vgpus in a host.
 * 
 * @author Ahmad Siavashi
 * 
 */
public abstract class VideoCardAllocationPolicy {

	/** vgpu to video card mapping */
	private Map<Vgpu, VideoCard> vgpuVideoCardMap;

	/** video cards attached to the host */
	private List<? extends VideoCard> videoCards = new ArrayList<VideoCard>();

	public VideoCardAllocationPolicy(List<? extends VideoCard> videoCards) {
		setVgpuVideoCardMap(new HashMap<Vgpu, VideoCard>());
		setVideoCards(videoCards);
	}

	/**
	 * Allocate PCIe bandwidth to the vgpu
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @param PCIeBw
	 *            amount of requested PCIe bandwidth
	 * @return $true is the request is accepted; $false otherwise.
	 */
	public abstract boolean allocate(Vgpu vgpu, int PCIeBw);

	/**
	 * @return the vgpuVideoCardMap
	 */
	public Map<Vgpu, VideoCard> getVgpuVideoCardMap() {
		return vgpuVideoCardMap;
	}

	/**
	 * @param vgpuVideoCardMap
	 *            the vgpuVideoCardMap to set
	 */
	protected void setVgpuVideoCardMap(Map<Vgpu, VideoCard> vgpuVideoCardMap) {
		this.vgpuVideoCardMap = vgpuVideoCardMap;
	}

	protected void setVideoCards(List<? extends VideoCard> videoCards) {
		this.videoCards = videoCards;
	}

	public List<? extends VideoCard> getVideoCards() {
		return videoCards;
	}

	/**
	 * Check if the vgpu can reside on the host associated to this video card
	 * allocation policy.
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @return $true if the vgpu can be allocated in the host associated with this
	 *         video card allocation policy; $false otherwise.
	 */
	public boolean isSuitable(Vgpu vgpu) {
		for (VideoCard videoCard : getVideoCards()) {
			boolean result = videoCard.getVgpuScheduler().isSuitable(vgpu);
			if (!result) {
				continue;
			}
			return true;
		}
		return false;
	}

	/**
	 * Deallocates resources allocated to a vgpu.
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @return $true if for a successful deallocation; $false otherwise.
	 */
	public boolean deallocate(Vgpu vgpu) {
		VideoCard videoCard = getVgpuVideoCardMap().get(vgpu);
		videoCard.getVgpuScheduler().deallocatePgpuForVgpu(vgpu);
		getVgpuVideoCardMap().remove(vgpu);
		return true;
	}
}
