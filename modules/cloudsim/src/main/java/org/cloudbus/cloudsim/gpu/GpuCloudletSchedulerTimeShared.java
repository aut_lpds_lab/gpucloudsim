/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Consts;
import org.cloudbus.cloudsim.ResCloudlet;
import org.cloudbus.cloudsim.core.CloudSim;

/**
 * {@link GpuCloudletSchedulerTimeShared} extends
 * {@link CloudletSchedulerTimeShared} to schedule {@link GpuCloudlet}s.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class GpuCloudletSchedulerTimeShared extends CloudletSchedulerTimeShared {

	/**
	 * Time Shared scheduler for GpuCloudlets. Assumes all PEs have same MIPS
	 * capacity.
	 */
	public GpuCloudletSchedulerTimeShared() {
	}

	@Override
	public double updateVmProcessing(double currentTime, List<Double> mipsShare) {
		setCurrentMipsShare(mipsShare);
		double timeSpam = currentTime - getPreviousTime();

		for (ResCloudlet rcl : getCloudletExecList()) {
			rcl.updateCloudletFinishedSoFar((long) (getTotalCurrentAvailableMipsForCloudlet(rcl, mipsShare)
					* getTotalUtilizationOfCpu(currentTime) * timeSpam * Consts.MILLION));
		}

		if (getCloudletExecList().size() == 0) {
			setPreviousTime(currentTime);
			return 0.0;
		}

		// check finished cloudlets
		double nextEvent = Double.MAX_VALUE;
		List<ResCloudlet> toRemove = new ArrayList<ResCloudlet>();
		for (ResCloudlet rcl : getCloudletExecList()) {
			long remainingLength = rcl.getRemainingCloudletLength();
			if (remainingLength == 0) {// finished: remove from the list
				toRemove.add(rcl);
				cloudletFinish(rcl);
				continue;
			}
		}
		getCloudletExecList().removeAll(toRemove);

		// estimate finish time of cloudlets
		for (ResCloudlet rcl : getCloudletExecList()) {
			double estimatedFinishTime = currentTime
					+ (rcl.getRemainingCloudletLength() / (getTotalCurrentAvailableMipsForCloudlet(rcl, mipsShare)));
			if (estimatedFinishTime - currentTime < CloudSim.getMinTimeBetweenEvents()) {
				estimatedFinishTime = currentTime + CloudSim.getMinTimeBetweenEvents();
			}

			if (estimatedFinishTime < nextEvent) {
				nextEvent = estimatedFinishTime;
			}
		}

		setPreviousTime(currentTime);
		return nextEvent;
	}

	// TODO: Test it
	@Override
	public double cloudletResume(int cloudletId) {
		boolean found = false;
		int position = 0;

		// look for the cloudlet in the paused list
		for (ResCloudlet rcl : getCloudletPausedList()) {
			if (rcl.getCloudletId() == cloudletId) {
				found = true;
				break;
			}
			position++;
		}

		if (found) {
			ResCloudlet rgl = getCloudletPausedList().remove(position);
			rgl.setCloudletStatus(Cloudlet.INEXEC);
			getCloudletExecList().add(rgl);

			// calculate the expected time for cloudlet completion
			// first: how many PEs do we have?

			double remainingLength = rgl.getRemainingCloudletLength();
			double estimatedFinishTime = CloudSim.clock()
					+ (remainingLength / getTotalCurrentAvailableMipsForCloudlet(rgl, getCurrentMipsShare()));

			return estimatedFinishTime;
		}

		return 0.0;
	}

	@Override
	public List<Double> getCurrentRequestedMips() {
		List<Double> mipsShare = new ArrayList<Double>();
		if (getCurrentMipsShare() != null) {
			double totalCpuUtilization = getTotalUtilizationOfCpu(CloudSim.clock());
			for (Double mips : getCurrentMipsShare()) {
				mipsShare.add(mips * totalCpuUtilization);
			}
		}
		return mipsShare;
	}

	@Override
	public double getTotalCurrentAvailableMipsForCloudlet(ResCloudlet rcl, List<Double> mipsShare) {
		double totalCurrentAvailableMipsForCloudlet = 0.0;
		double mipsCapacity = getCapacity(mipsShare);
		for (int i = 0; i < rcl.getNumberOfPes(); i++) {
			totalCurrentAvailableMipsForCloudlet += mipsCapacity;
		}
		return totalCurrentAvailableMipsForCloudlet;
	}

	@Override
	public double getTotalCurrentAllocatedMipsForCloudlet(ResCloudlet rcl, double time) {
		double totalCurrentAllocatedMipsForCloudlet = 0.0;
		double mipsCapacity = getCapacity(getCurrentMipsShare());
		for (int i = 0; i < rcl.getNumberOfPes(); i++) {
			totalCurrentAllocatedMipsForCloudlet += mipsCapacity;
		}
		return totalCurrentAllocatedMipsForCloudlet;
	}

	@Override
	public double getTotalCurrentRequestedMipsForCloudlet(ResCloudlet rcl, double time) {
		return rcl.getCloudlet().getUtilizationOfCpu(time) * getTotalCurrentAllocatedMipsForCloudlet(rcl, time);
	}

	@Override
	public double getTotalUtilizationOfCpu(double time) {
		double totalUtilizationOfCpu = super.getTotalUtilizationOfCpu(time);
		return totalUtilizationOfCpu > 1 ? 1 : totalUtilizationOfCpu;
	}

	@Override
	public double getCurrentRequestedUtilizationOfRam() {
		double currentRequestedUtilizationOfRam = super.getCurrentRequestedUtilizationOfRam();
		return currentRequestedUtilizationOfRam > 1 ? 1 : currentRequestedUtilizationOfRam;
	}

	@Override
	public double getCurrentRequestedUtilizationOfBw() {
		double currentRequestedUtilizationOfBw = super.getCurrentRequestedUtilizationOfBw();
		return currentRequestedUtilizationOfBw > 1 ? 1 : currentRequestedUtilizationOfBw;
	}

}
