/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

import org.cloudbus.cloudsim.gpu.interference.InterferenceGpuTaskSchedulerLeftover;
import org.cloudbus.cloudsim.gpu.interference.models.InterferenceModel;
import org.cloudbus.cloudsim.gpu.interference.models.InterferenceModelGpuMemory;

/**
 * 
 * Methods & constants that are related to {@link GpuVm GpuVms} types and configurations.
 * 
 * @author Ahmad Siavashi
 *
 */
public class GpuVmTags {

	public static final int GPU_VM_CUSTOM = -1;
	public static final int VCPU_8_E5_2090_V4_RAM_16_K280Q = 0;
	public static final int VCPU_4_E5_2620_V3_RAM_16_K180Q = 1;
	public static final int VCPU_4_E5_2090_V4_RAM_8_K260Q = 2;
	public static final int VCPU_2_E5_2620_V3_RAM_8_K160Q = 3;
	public static final int VCPU_2_E5_2090_V4_RAM_4_K240Q = 4;
	public static final int VCPU_2_E5_2620_V3_RAM_4_K140Q = 5;

	public static GpuVm getGpuVmInstance(int vmId, int brokerId, int type) {
		/** VM description */
		double mips = 0.0;
		// image size (GB)
		int size = 25;
		// vm memory (GB)
		int ram = 0;
		long bw = 100;
		// number of cpus
		int pesNumber = 0;
		// VMM name
		String vmm = "vSphere";
		// Vgpu Id
		int vgpuId = vmId;
		// Interference Model
		InterferenceModel<ResGpuTask> interferenceModel = new InterferenceModelGpuMemory();
		// Create the GpuTask Scheduler
		InterferenceGpuTaskSchedulerLeftover gpuTaskScheduler = new InterferenceGpuTaskSchedulerLeftover(
				interferenceModel);
		Vgpu vgpu = null;
		switch (type) {
		case VCPU_8_E5_2090_V4_RAM_16_K280Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2690_V4_PE_MIPS;
			pesNumber = 8;
			ram = 16;
			vgpu = GridVgpuTags.getK280Q(vgpuId, gpuTaskScheduler);
			break;
		case VCPU_4_E5_2620_V3_RAM_16_K180Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2620_V3_PE_MIPS;
			pesNumber = 4;
			ram = 16;
			vgpu = GridVgpuTags.getK180Q(vgpuId, gpuTaskScheduler);
			break;
		case VCPU_4_E5_2090_V4_RAM_8_K260Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2690_V4_PE_MIPS;
			pesNumber = 4;
			ram = 8;
			vgpu = GridVgpuTags.getK260Q(vgpuId, gpuTaskScheduler);
			break;
		case VCPU_2_E5_2620_V3_RAM_8_K160Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2620_V3_PE_MIPS;
			pesNumber = 2;
			ram = 8;
			vgpu = GridVgpuTags.getK160Q(vgpuId, gpuTaskScheduler);
			break;
		case VCPU_2_E5_2090_V4_RAM_4_K240Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2690_V4_PE_MIPS;
			pesNumber = 2;
			ram = 4;
			vgpu = GridVgpuTags.getK240Q(vgpuId, gpuTaskScheduler);
			break;
		case VCPU_2_E5_2620_V3_RAM_4_K140Q:
			mips = GpuHostTags.DUAL_INTEL_XEON_E5_2620_V3_PE_MIPS;
			pesNumber = 2;
			ram = 4;
			vgpu = GridVgpuTags.getK140Q(vgpuId, gpuTaskScheduler);
			break;
		default:
			System.err.println("[!] Unknown GpuVm Type: " + type);
			System.exit(1);
			break;
		}
		// Create VM
		GpuVm newVm = new GpuVm(vmId, brokerId, mips, pesNumber, ram, bw, size, vmm, type,
				new GpuCloudletSchedulerTimeShared());
		newVm.setVgpu(vgpu);
		return newVm;
	}

	public static String getGpuVmTypeString(int type) {
		switch (type) {
		case VCPU_8_E5_2090_V4_RAM_16_K280Q:
			return "VCPU_8_E5_2090_V4_RAM_16_K280Q";
		case VCPU_4_E5_2620_V3_RAM_16_K180Q:
			return "VCPU_4_E5_2620_V3_RAM_16_K180Q";
		case VCPU_4_E5_2090_V4_RAM_8_K260Q:
			return "VCPU_4_E5_2090_V4_RAM_8_K260Q";
		case VCPU_2_E5_2620_V3_RAM_8_K160Q:
			return "VCPU_2_E5_2620_V3_RAM_8_K160Q";
		case VCPU_2_E5_2090_V4_RAM_4_K240Q:
			return "VCPU_2_E5_2090_V4_RAM_4_K240Q";
		case VCPU_2_E5_2620_V3_RAM_4_K140Q:
			return "VCPU_2_E5_2620_V3_RAM_4_K140Q";
		default:
			return "Custom";
		}
	}

	/**
	 * Cannot be instantiated (Singleton).
	 */
	private GpuVmTags() {

	}

}
