/**
 * 
 */
package org.cloudbus.cloudsim.gpu.selection;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cloudbus.cloudsim.gpu.Pgpu;
import org.cloudbus.cloudsim.gpu.VgpuScheduler;
import org.cloudbus.cloudsim.lists.PeList;

/**
 * {@link PgpuSelectionPolicyMaxMipsFirst} implements
 * {@link PgpuSelectionPolicy} and selects the Pgpu with the maximum mips.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class PgpuSelectionPolicyMaxMipsFirst implements PgpuSelectionPolicy {

	/**
	 * Returns the pgpu with the maximum mips.
	 */
	public PgpuSelectionPolicyMaxMipsFirst() {
	}

	@Override
	public <T extends VgpuScheduler> Pgpu selectPgpu(T scheduler, final List<? extends Pgpu> pgpuList) {
		if (pgpuList.isEmpty()) {
			return null;
		}
		return Collections.max(pgpuList, new Comparator<Pgpu>() {
			public int compare(Pgpu pgpu1, Pgpu pgpu2) {
				double pgpu1Mips = PeList.getTotalMips(pgpu1.getPeList());
				double pgpu2Mips = PeList.getTotalMips(pgpu2.getPeList());
				return Double.compare(pgpu1Mips, pgpu2Mips);
			}
		});
	}
}
