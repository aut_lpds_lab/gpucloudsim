package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.gpu.GpuTask.MemoryTransfer;
import org.cloudbus.cloudsim.gpu.allocation.VideoCardAllocationPolicy;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;

/**
 * 
 * {@link GpuHost} extends {@link Host} and supports {@link VideoCard}s through
 * a {@link VideoCardAllocationPolicy}.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class GpuHost extends Host {

	/**
	 * type of the host
	 */
	public int type;

	/** video card allocation policy */
	private VideoCardAllocationPolicy videoCardAllocationPolicy;

	/**
	 * 
	 * See {@link Host#Host}
	 * 
	 * @param type
	 *            type of the host which is specified in {@link GpuHostTags}.
	 * @param videoCardAllocationPolicy
	 *            the policy in which the host allocates video cards to vms
	 */
	public GpuHost(int id, int type, RamProvisioner ramProvisioner, BwProvisioner bwProvisioner, long storage,
			List<? extends Pe> peList, VmScheduler vmScheduler, VideoCardAllocationPolicy videoCardAllocationPolicy) {
		super(id, ramProvisioner, bwProvisioner, storage, peList, vmScheduler);
		setType(type);
		setVideoCardAllocationPolicy(videoCardAllocationPolicy);
	}

	/**
	 * 
	 * See {@link Host#Host}
	 * 
	 * @param type
	 *            type of the host which is specified in {@link GpuHostTags}.
	 */
	public GpuHost(int id, int type, RamProvisioner ramProvisioner, BwProvisioner bwProvisioner, long storage,
			List<? extends Pe> peList, VmScheduler vmScheduler) {
		super(id, ramProvisioner, bwProvisioner, storage, peList, vmScheduler);
		setType(type);
	}

	@Override
	public boolean isSuitableForVm(Vm vm) {
		// Checking host resources
		boolean hasStorage = getStorage() >= vm.getSize();
		boolean hasRam = getRamProvisioner().isSuitableForVm(vm, vm.getCurrentRequestedRam());
		boolean hasBw = getBwProvisioner().isSuitableForVm(vm, vm.getCurrentRequestedBw());
		boolean hasMips = getVmScheduler().getPeCapacity() >= vm.getCurrentRequestedMaxMips()
				&& getVmScheduler().getAvailableMips() >= vm.getCurrentRequestedTotalMips();
		if (!hasStorage || !hasRam || !hasBw || !hasMips) {
			return false;
		}
		// Checking GPU resources of the host
		Vgpu vgpu = ((GpuVm) vm).getVgpu();
		// if the VM has no vGPU -> return true.
		if (vgpu == null) {
			return true;
		}
		// if the VM has a vGPU and the host has no local video card -> return false.
		else if (getVideoCardAllocationPolicy() == null) {
			return false;
		}
		// if the VM has a vGPU and the host has video card(s) -> check compatibility.
		return getVideoCardAllocationPolicy().isSuitable(vgpu);
	}

	@Override
	public boolean vmCreate(Vm vm) {
		// Allocation of host resources
		if (getStorage() < vm.getSize()) {
			Log.printConcatLine("[VmScheduler.vmCreate] Allocation of VM #", vm.getId(), " to Host #", getId(),
					" failed by storage");
			return false;
		}

		if (!getRamProvisioner().allocateRamForVm(vm, vm.getCurrentRequestedRam())) {
			Log.printConcatLine("[VmScheduler.vmCreate] Allocation of VM #", vm.getId(), " to Host #", getId(),
					" failed by RAM");
			return false;
		}

		if (!getBwProvisioner().allocateBwForVm(vm, vm.getCurrentRequestedBw())) {
			Log.printConcatLine("[VmScheduler.vmCreate] Allocation of VM #", vm.getId(), " to Host #", getId(),
					" failed by BW");
			getRamProvisioner().deallocateRamForVm(vm);
			return false;
		}

		if (!getVmScheduler().allocatePesForVm(vm, vm.getCurrentRequestedMips())) {
			Log.printConcatLine("[VmScheduler.vmCreate] Allocation of VM #", vm.getId(), " to Host #", getId(),
					" failed by MIPS");
			getRamProvisioner().deallocateRamForVm(vm);
			getBwProvisioner().deallocateBwForVm(vm);
			return false;
		}

		setStorage(getStorage() - vm.getSize());

		// Device (GPU) allocation
		Vgpu vgpu = ((GpuVm) vm).getVgpu();

		// if the VM has no vGPU -> success.
		if (vgpu == null) {
			getVmList().add(vm);
			vm.setHost(this);
			return true;
		}
		// if the VM has a vGPU but the host has no local video card -> fail.
		else if (getVideoCardAllocationPolicy() == null) {
			rollbackHostResourceAllocation(vm);
			return false;
		}
		// if the VM has a vGPU and the host has local video card(s) -> check
		boolean isVgpuAllocated = getVideoCardAllocationPolicy().allocate(vgpu, vgpu.getPCIeBw());
		// if vGPU allocation failed -> fail.
		if (!isVgpuAllocated) {
			Log.printConcatLine("[VmScheduler.vmCreate] Allocation of GPU accelerated VM #", vm.getId(), " to Host #",
					getId(), " failed due to vgpu allocation failure.");
			rollbackHostResourceAllocation(vm);
			return false;
		} // else -> success
		getVmList().add(vm);
		vm.setHost(this);
		return true;
	}

	/**
	 * Deallocation of host resources for a given vm
	 * 
	 * @param vm
	 *            the vm
	 */
	protected void rollbackHostResourceAllocation(Vm vm) {
		getRamProvisioner().deallocateRamForVm(vm);
		getBwProvisioner().deallocateBwForVm(vm);
		getVmScheduler().deallocatePesForVm(vm);
		setStorage(getStorage() + vm.getSize());
	}

	@Override
	protected void vmDeallocate(Vm vm) {
		// Vm deallocation */
		rollbackHostResourceAllocation(vm);
		// Vgpu deallocation
		if (getVideoCardAllocationPolicy() != null) {
			getVideoCardAllocationPolicy().deallocate(((GpuVm) vm).getVgpu());
		}
		// vm removal
		getVmList().remove(vm);
	}

	@Override
	public double updateVmsProcessing(double currentTime) {
		double smallerTime = Double.MAX_VALUE;
		// Update resident VMs
		for (Vm vm : getVmList()) {
			double time = vm.updateVmProcessing(currentTime, getVmScheduler().getAllocatedMipsForVm(vm));
			if (time > 0.0 && time < smallerTime) {
				smallerTime = time;
			}
		}
		if (getVideoCardAllocationPolicy() != null) {
			// Update resident vGPUs
			for (Vgpu vgpu : getVideoCardAllocationPolicy().getVgpuVideoCardMap().keySet()) {
				double time = vgpu.updateTaskProcessing(currentTime, getVideoCardAllocationPolicy()
						.getVgpuVideoCardMap().get(vgpu).getVgpuScheduler().getAllocatedMipsForVgpu(vgpu));
				if (time > 0.0 && time < smallerTime) {
					smallerTime = time;
				}
			}
		}

		return smallerTime;
	}

	protected double updateVgpusMemoryTransfer(double currentTime) {
		double smallerTime = Double.MAX_VALUE;
		Map<Pgpu, List<Vgpu>> candidates = new HashMap<Pgpu, List<Vgpu>>();
		Map<Pgpu, VideoCard> candidatePgpuVideoCardMap = new HashMap<Pgpu, VideoCard>();
		for (VideoCard videoCard : getVideoCardAllocationPolicy().getVideoCards()) {
			videoCard.getPCIeBandwidthProvisioner().deallocateBwForAllPgpus();
			for (Entry<Pgpu, List<Vgpu>> entry : videoCard.getVgpuScheduler().getPgpuVgpuMap().entrySet()) {
				Pgpu pgpu = entry.getKey();
				List<Vgpu> vgpuList = entry.getValue();
				// Each pgpu has only two copy engines.
				// One engine is for host to device memory transfers and the other works in the
				// opposite direction.
				boolean hasHostToDeviceMemoryTransfer = false;
				boolean hasDeviceToHostMemoryTransfer = false;
				List<Vgpu> vgpusWithMemoryTransfer = new ArrayList<Vgpu>();
				for (Vgpu vgpu : vgpuList) {
					if (!hasHostToDeviceMemoryTransfer && vgpu.getGpuTaskScheduler()
							.memoryTransferDirection() == MemoryTransfer.MEMORY_TRANSFER_HOST_TO_DEVICE) {
						vgpusWithMemoryTransfer.add(vgpu);
						hasHostToDeviceMemoryTransfer = true;
					} else if (!hasDeviceToHostMemoryTransfer && vgpu.getGpuTaskScheduler()
							.memoryTransferDirection() == MemoryTransfer.MEMORY_TRANSFER_DEVICE_TO_HOST) {
						vgpusWithMemoryTransfer.add(vgpu);
						hasDeviceToHostMemoryTransfer = true;
					}
					if (hasHostToDeviceMemoryTransfer && hasDeviceToHostMemoryTransfer) {
						break;
					}
				}
				if (hasHostToDeviceMemoryTransfer || hasDeviceToHostMemoryTransfer) {
					candidates.put(pgpu, vgpusWithMemoryTransfer);
					videoCard.getPCIeBandwidthProvisioner().allocateBwForPgpu(pgpu, VideoCardTags.PCI_E_3_X16_BW);
					candidatePgpuVideoCardMap.put(pgpu, videoCard);
				}
			}
		}
		for (Entry<Pgpu, List<Vgpu>> entry : candidates.entrySet()) {
			Pgpu pgpu = entry.getKey();
			VideoCard videoCard = candidatePgpuVideoCardMap.get(pgpu);
			for (Vgpu vgpu : entry.getValue()) {
				long pgpuBw = videoCard.getPCIeBandwidthProvisioner().getAllocatedBwForPgpu(entry.getKey());
				double time = vgpu.updateTaskMemoryTransfer(currentTime, pgpuBw);
				if (time > 0.0 && time < smallerTime) {
					smallerTime = time;
				}
			}
		}
		return smallerTime;
	}

	/**
	 * @return the videoCardAllocationPolicy
	 */
	public VideoCardAllocationPolicy getVideoCardAllocationPolicy() {
		return videoCardAllocationPolicy;
	}

	/**
	 * @param videoCardAllocationPolicy
	 *            the videoCardAllocationPolicy to set
	 */
	public void setVideoCardAllocationPolicy(VideoCardAllocationPolicy videoCardAllocationPolicy) {
		this.videoCardAllocationPolicy = videoCardAllocationPolicy;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	protected void setType(int type) {
		this.type = type;
	}
}
