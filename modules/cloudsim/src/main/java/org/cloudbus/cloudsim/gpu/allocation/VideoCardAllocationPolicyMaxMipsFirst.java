package org.cloudbus.cloudsim.gpu.allocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.gpu.Pgpu;
import org.cloudbus.cloudsim.gpu.Vgpu;
import org.cloudbus.cloudsim.gpu.VideoCard;
import org.cloudbus.cloudsim.lists.PeList;

/**
 * {@link VideoCardAllocationPolicyMaxMipsFirst} extends
 * {@link VideoCardAllocationPolicy} to provision video cards to vgpus on a
 * host. Host video cards are sorted in descending order from the the video card
 * with fastest pgpu to the video card with the slowest pgpu. Then, they are
 * traversed one by one until the newly arrived vgpu is allocated.
 * 
 * @author Ahmad Siavashi
 *
 */
public class VideoCardAllocationPolicyMaxMipsFirst extends VideoCardAllocationPolicySimple {

	private Map<VideoCard, List<Vgpu>> videoCardVgpuMap;

	/**
	 * @param videoCards
	 */
	public VideoCardAllocationPolicyMaxMipsFirst(List<? extends VideoCard> videoCards) {
		super(videoCards);
		setVideoCardVgpuMap(new HashMap<>());
		for (VideoCard videoCard : getVideoCards()) {
			getVideoCardVgpuMap().put(videoCard, new ArrayList<>());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cloudbus.cloudsim.gpu.allocation.VideoCardAllocationPolicy#allocate(org.
	 * cloudbus.cloudsim.gpu.Vgpu, int)
	 */
	@Override
	public boolean allocate(Vgpu vgpu, int PCIeBw) {
		Collections.sort(getVideoCards(), Collections.reverseOrder(new Comparator<VideoCard>() {
			@Override
			public int compare(VideoCard videoCard1, VideoCard videoCard2) {
				List<Integer> videoCard1PgpuMips = new ArrayList<>();
				List<Integer> videoCard2PgpuMips = new ArrayList<>();
				for (Pgpu pgpu : videoCard1.getVgpuScheduler().getPgpuList()) {
					videoCard1PgpuMips.add(PeList.getTotalMips(pgpu.getPeList()));
				}
				for (Pgpu pgpu : videoCard2.getVgpuScheduler().getPgpuList()) {
					videoCard2PgpuMips.add(PeList.getTotalMips(pgpu.getPeList()));
				}
				return Integer.compare(Collections.max(videoCard1PgpuMips), Collections.max(videoCard1PgpuMips));
			}
		}));
		boolean result = super.allocate(vgpu, PCIeBw);
		if (result) {
			VideoCard videoCard = getVgpuVideoCardMap().get(vgpu);
			getVideoCardVgpuMap().get(videoCard).add(vgpu);
		}
		return result;
	}

	@Override
	public boolean deallocate(Vgpu vgpu) {
		VideoCard videoCard = getVgpuVideoCardMap().get(vgpu);
		getVideoCardVgpuMap().get(videoCard).remove(vgpu);
		return super.deallocate(vgpu);
	}

	/**
	 * @return the videoCardVgpuMap
	 */
	protected Map<VideoCard, List<Vgpu>> getVideoCardVgpuMap() {
		return videoCardVgpuMap;
	}

	/**
	 * @param videoCardVgpuMap
	 *            the videoCardVgpuMap to set
	 */
	protected void setVideoCardVgpuMap(Map<VideoCard, List<Vgpu>> videoCardVgpuMap) {
		this.videoCardVgpuMap = videoCardVgpuMap;
	}

}
