/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

/**
 * 
 * Methods & constants that are related to {@link Vgpu virtual gpus} types and configurations.
 * 
 * @author Ahmad Siavashi
 * 
 */

public class GridVgpuTags {

	public final static int VGPU_CUSTOM = -1;

	/** NVIDIA GRID K1 Profiles */
	public final static int MAX_K120Q_VGPUS_PER_K1_PGPU = 8;
	public final static int MAX_K140Q_VGPUS_PER_K1_PGPU = 4;
	public final static int MAX_K160Q_VGPUS_PER_K1_PGPU = 2;
	public final static int MAX_K180Q_VGPUS_PER_K1_PGPU = 1;

	public final static int K1_K120Q = 1;
	public final static int K1_K140Q = 2;
	public final static int K1_K160Q = 3;
	public final static int K1_K180Q = 4;

	public final static int[] K1_VGPUS = { K1_K120Q, K1_K140Q, K1_K160Q, K1_K180Q };

	/** NVIDIA GRID K2 Profiles */
	public final static int MAX_K220Q_VGPUS_PER_K2_PGPU = 8;
	public final static int MAX_K240Q_VGPUS_PER_K2_PGPU = 4;
	public final static int MAX_K260Q_VGPUS_PER_K2_PGPU = 2;
	public final static int MAX_K280Q_VGPUS_PER_K2_PGPU = 1;

	public final static int K2_K220Q = 5;
	public final static int K2_K240Q = 6;
	public final static int K2_K260Q = 7;
	public final static int K2_K280Q = 8;

	public final static int[] K2_VGPUS = { K2_K220Q, K2_K240Q, K2_K260Q, K2_K280Q };

	public final static int[] PASS_THROUGH_VGPUS = { K1_K180Q, K2_K280Q };

	/**
	 * K1 Board Pass through type 1/pGPU, 4/board
	 * 
	 * @return a GK107 physical GPU (pass through)
	 */
	public static Vgpu getK180Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K1_K180Q;
		// GPU Clock: 850 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K1_CARD, 850);
		// SMX count: 1
		final int numberOfPes = 1;
		// GDDRAM: 256 MB
		final int gddram = 4096;
		// Bandwidth: 28.5 GB/s
		final long bw = (long) 28.5 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K1 Board K160Q vGPU type 2/pGPU, 8/board
	 * 
	 * @return a K140Q virtual GPU
	 */
	public static Vgpu getK160Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K1_K160Q;
		// GPU Clock: 850 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K1_CARD, 850);
		// SMX count: 1
		final int numberOfPes = 1;
		// GDDRAM: 2 GB
		final int gddram = 2048;
		// Bandwidth: 28.5 GB/s
		final long bw = (long) 28.5 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K1 Board K140Q vGPU type 4/pGPU, 16/board
	 * 
	 * @return a K140Q virtual GPU
	 */
	public static Vgpu getK140Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K1_K140Q;
		// GPU Clock: 850 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K1_CARD, 850);
		// SMX count: 1
		final int numberOfPes = 1;
		// GDDRAM: 256 MB
		final int gddram = 1024;
		// Bandwidth: 28.5 GB/s
		final long bw = (long) 28.5 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K1 Board K120Q vGPU type 8/pGPU, 32/board
	 * 
	 * @return a K120Q virtual GPU
	 */
	public static Vgpu getK120Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K1_K120Q;
		// GPU Clock: 850 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K1_CARD, 850);
		// SMX count: 1
		final int numberOfPes = 1;
		// GDDRAM: 512 MB
		final int gddram = 512;
		// Bandwidth: 28.5 GB/s
		final long bw = (long) 28.5 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K2 Board K220Q vGPU type 8/pGPU, 16/board
	 * 
	 * @return a K220Q virtual GPU
	 */
	public static Vgpu getK220Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K2_K220Q;
		// GPU Clock: 745 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K2_CARD, 745);
		// SMX count: 8
		final int numberOfPes = 8;
		// GDDRAM: 512 MB
		final int gddram = 512;
		// Bandwidth: 160 GB/s
		final long bw = 160 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K2 Board K240Q vGPU type 4/pGPU, 8/board
	 * 
	 * @return a K240Q virtual GPU
	 */
	public static Vgpu getK240Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K2_K240Q;
		// GPU Clock: 745 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K2_CARD, 745);
		// SMX count: 8
		final int numberOfPes = 8;
		// GDDRAM: 1 GB
		final int gddram = 1024;
		// Bandwidth: 160 GB/s
		final long bw = 160 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K2 Board K260Q vGPU type 2/pGPU, 4/board
	 * 
	 * @return a K260Q virtual GPU
	 */
	public static Vgpu getK260Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K2_K260Q;
		// GPU Clock: 745 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K2_CARD, 745);
		// SMX count: 8
		final int numberOfPes = 8;
		// GDDRAM: 2 GB
		final int gddram = 2048;
		// Bandwidth: 160 GB/s
		final long bw = 160 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	/**
	 * K2 Board K280Q vGPU type 1/pGPU, 2/board
	 * 
	 * @return a K280Q virtual GPU
	 */
	public static Vgpu getK280Q(int vgpuId, GpuTaskScheduler scheduler) {
		final int type = K2_K280Q;
		// GPU Clock: 745 MHz
		final double mips = VideoCardTags.getGpuPeMipsFromFrequency(VideoCardTags.NVIDIA_K2_CARD, 745);
		// SMX count: 8
		final int numberOfPes = 8;
		// GDDRAM: 4 GB
		final int gddram = 4096;
		// Bandwidth: 160 GB/s
		final long bw = 160 * 1024;
		Vgpu vgpu = new Vgpu(vgpuId, mips, numberOfPes, gddram, bw, type, scheduler, VideoCardTags.PCI_E_3_X16_BW);
		return vgpu;
	}

	public static String getVgpuTypeString(int type) {
		switch (type) {
		case K1_K120Q:
			return "K120Q";
		case K1_K140Q:
			return "K140Q";
		case K1_K160Q:
			return "K160Q";
		case K1_K180Q:
			return "K180Q";
		case K2_K220Q:
			return "K220Q";
		case K2_K240Q:
			return "K240Q";
		case K2_K260Q:
			return "K260Q";
		case K2_K280Q:
			return "K280Q";
		default:
			return "Custom";
		}
	}

	/**
	 * Singleton class (cannot be instantiated)
	 */
	private GridVgpuTags() {
	}

}
