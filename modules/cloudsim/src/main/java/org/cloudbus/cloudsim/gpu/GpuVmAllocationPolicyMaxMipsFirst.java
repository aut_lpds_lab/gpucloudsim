package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.lists.PeList;

/**
 * {@link GpuVmAllocationPolicyMaxMipsFirst} extends
 * {@link GpuVmAllocationPolicySimple} to implement a VM placement algorithm.
 * Initially, it sorts {@link GpuHost GpuHosts} based on their fastest
 * {@link Pgpu GPU}, then employes first-fit algorithm to place VMs. sorts
 * 
 * @author Ahmad Siavashi
 *
 */
public class GpuVmAllocationPolicyMaxMipsFirst extends GpuVmAllocationPolicySimple {

	public GpuVmAllocationPolicyMaxMipsFirst(List<? extends Host> list) {
		super(list);
		Collections.sort(getHostList(), Collections.reverseOrder(new Comparator<GpuHost>() {
			public int compare(GpuHost host1, GpuHost host2) {
				List<Integer> host1Gpus = new ArrayList<>();
				for (VideoCard videoCard : host1.getVideoCardAllocationPolicy().getVideoCards()) {
					for (Pgpu pgpu : videoCard.getVgpuScheduler().getPgpuList()) {
						host1Gpus.add(PeList.getTotalMips(pgpu.getPeList()));
					}
				}
				List<Integer> host2Gpus = new ArrayList<>();
				for (VideoCard videoCard : host2.getVideoCardAllocationPolicy().getVideoCards()) {
					for (Pgpu pgpu : videoCard.getVgpuScheduler().getPgpuList()) {
						host2Gpus.add(PeList.getTotalMips(pgpu.getPeList()));
					}
				}
				return Integer.compare(Collections.max(host1Gpus), Collections.max(host2Gpus));
			};
		}));
	}

}
