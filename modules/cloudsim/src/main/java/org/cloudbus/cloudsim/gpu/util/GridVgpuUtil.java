package org.cloudbus.cloudsim.gpu.util;

import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;
import org.cloudbus.cloudsim.gpu.Pgpu;
import org.cloudbus.cloudsim.gpu.Vgpu;
import org.cloudbus.cloudsim.gpu.GridVgpuTags;
import org.cloudbus.cloudsim.gpu.VideoCardTags;

/**
 * A class containing multiple convenient functions to work with GRID Vgpus.
 * 
 * @author Ahmad Siavashi
 *
 */
public class GridVgpuUtil {

	/**
	 * Checks whether a videoCard type supports a given vgpu type or not.
	 * 
	 * @param videoCardType
	 *            type of the videoCard
	 * @param vgpuType
	 *            type of the vgpu
	 * @return $true if the videoCard supports the given vgpu type; $false
	 *         otherwise.
	 */
	public static boolean isVideoCardSuitable(int videoCardType, int vgpuType) {
		switch (videoCardType) {
		case VideoCardTags.NVIDIA_K1_CARD:
			return ArrayUtils.contains(GridVgpuTags.K1_VGPUS, vgpuType);
		case VideoCardTags.NVIDIA_K2_CARD:
			return ArrayUtils.contains(GridVgpuTags.K2_VGPUS, vgpuType);
		default:
			return true;
		}
	}

	/**
	 * Checks whether it is possible to allocate the given vgpu on any of the given
	 * pgpus or not.
	 * 
	 * @param currentResidents
	 *            videoCard's pgpus with their current resident vgpus
	 * @param newVgpu
	 *            the newly arrived vgpu
	 * @return $true if a suitable pgpu exists for <b>newVgpu</b>; $false otherwise
	 */
	public static boolean isPgpuSuitable(Entry<Pgpu, List<Vgpu>> currentResidents, Vgpu newVgpu) {
		Pgpu pgpu = currentResidents.getKey();
		List<Vgpu> vgpus = currentResidents.getValue();
		if (vgpus.isEmpty()) {
			return true;
		} else if (vgpus.get(0).getType() != newVgpu.getType()) {
			return false;
		}
		int currentNumberOfVgpus = vgpus.size();
		switch (newVgpu.getType()) {
		case GridVgpuTags.K1_K120Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K120Q_VGPUS_PER_K1_PGPU;
		case GridVgpuTags.K1_K140Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K140Q_VGPUS_PER_K1_PGPU;
		case GridVgpuTags.K1_K160Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K160Q_VGPUS_PER_K1_PGPU;
		case GridVgpuTags.K1_K180Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K180Q_VGPUS_PER_K1_PGPU;
		case GridVgpuTags.K2_K220Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K220Q_VGPUS_PER_K2_PGPU;
		case GridVgpuTags.K2_K240Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K240Q_VGPUS_PER_K2_PGPU;
		case GridVgpuTags.K2_K260Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K260Q_VGPUS_PER_K2_PGPU;
		case GridVgpuTags.K2_K280Q:
			return currentNumberOfVgpus < GridVgpuTags.MAX_K280Q_VGPUS_PER_K2_PGPU;
		}
		if (pgpu.getGddramProvisioner().isSuitableForVgpu(newVgpu, newVgpu.getGddram())
				&& pgpu.getBwProvisioner().isSuitableForVgpu(newVgpu, newVgpu.getBw())) {
			return true;
		}
		return false;
	}

	/**
	 * Checks the type of the given vgpu to see if it is a pass-through type.
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @return $true if the vgpu type is a pass-through type; $false otherwise.
	 */
	public static boolean isPassThrough(Vgpu vgpu) {
		return ArrayUtils.contains(GridVgpuTags.PASS_THROUGH_VGPUS, vgpu.getType());
	}

	/**
	 * Cannot be instantiated (Singleton).
	 */
	private GridVgpuUtil() {
	}

}
