package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.gpu.selection.PgpuSelectionPolicy;
import org.cloudbus.cloudsim.gpu.util.GridVgpuUtil;

/**
 * VgpuSchedulerSpaceShared is a vgpu allocation policy that allocates one or
 * more pgpu PEs from a video card to a vgpu, and doesn't allow sharing of
 * pgpus' PEs. The allocated PEs will be used until the vgpu finishes running.
 * If there is no enough free PEs as required by a vgpu, or whether the
 * available PEs doesn't have enough capacity, the allocation fails. In the case
 * of fail, no PE is allocated to the requesting vgpu.
 * 
 * @author Ahmad Siavashi
 */
public class VgpuSchedulerSpaceShared extends VgpuScheduler {

	/**
	 * Instantiates a new vgpu space-shared scheduler.
	 * 
	 * @param videoCardType
	 *            type of the video card associated with this vgpuScheduler
	 * @param pgpuList
	 *            list of video card's pgpus
	 * @param pgpuSelectionPolicy
	 *            vgpu to pgpu allocation policy
	 */
	public VgpuSchedulerSpaceShared(int videoCardType, List<Pgpu> pgpuList, PgpuSelectionPolicy pgpuSelectionPolicy) {
		super(videoCardType, pgpuList, pgpuSelectionPolicy);
	}

	/**
	 * Checks whether the vgpu type is supported by this video card type or not.
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @return $true if the video card supports the vgpu type.
	 */
	protected boolean isVideoCardSuitableForVgpu(Vgpu vgpu) {
		if (!GridVgpuUtil.isVideoCardSuitable(getVideoCardType(), vgpu.getType())) {
			return false;
		}
		for (Entry<Pgpu, List<Vgpu>> entry : getPgpuVgpuMap().entrySet()) {
			if (GridVgpuUtil.isPgpuSuitable(entry, vgpu)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isSuitable(final Vgpu vgpu) {
		if (!isVideoCardSuitableForVgpu(vgpu)) {
			return false;
		}
		final List<Double> mipsShare = vgpu.getCurrentRequestedMips();
		final int gddramShare = vgpu.getCurrentRequestedGddram();
		final long bwShare = vgpu.getCurrentRequestedBw();
		List<Pgpu> candidates = (List<Pgpu>) CollectionUtils.select(getPgpuList(), new Predicate() {
			@Override
			public boolean evaluate(Object arg) {
				Pgpu pgpu = (Pgpu) arg;
				if (!pgpu.getGddramProvisioner().isSuitableForVgpu(vgpu, gddramShare)
						|| !pgpu.getBwProvisioner().isSuitableForVgpu(vgpu, bwShare)) {
					return false;
				}
				List<Pe> pgpuPes = pgpu.getPeList();
				int freePes = CollectionUtils.countMatches(pgpuPes, new Predicate() {
					@Override
					public boolean evaluate(Object arg) {
						Pe pe = (Pe) arg;
						if (pe.getPeProvisioner().getTotalAllocatedMips() == 0) {
							return true;
						}
						return false;
					}
				});
				if (freePes < mipsShare.size()) {
					return false;
				}
				// PEs in the pgpu are sorted according to their
				// available mips
				Collections.sort(pgpuPes, Collections.reverseOrder(new Comparator<Pe>() {
					public int compare(Pe pe1, Pe pe2) {
						return Double.compare(pe1.getPeProvisioner().getAvailableMips(),
								pe2.getPeProvisioner().getAvailableMips());
					}
				}));
				for (int i = 0; i < mipsShare.size(); i++) {
					if (mipsShare.get(i) > pgpuPes.get(i).getPeProvisioner().getAvailableMips()) {
						return false;
					}
				}
				return true;
			}
		});

		// if there is no candidate,
		if (candidates.isEmpty()) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean allocatePgpuForVgpu(final Vgpu vgpu, final List<Double> mipsShare, final int gddramShare,
			final long bwShare) {
		if (!isVideoCardSuitableForVgpu(vgpu)) {
			return false;
		}
		final Vm vm = vgpu.getVm();
		// TODO: A worst-fit strategy is taken now, other strategies can be
		// employes as well
		// in order to consider process variation
		// ... mipsShare is sorted first in descending order
		Collections.sort(mipsShare, Collections.reverseOrder());

		List<Pgpu> candidates = (List<Pgpu>) CollectionUtils.select(getPgpuList(), new Predicate() {
			@Override
			public boolean evaluate(Object arg) {
				Pgpu pgpu = (Pgpu) arg;
				if (!pgpu.getGddramProvisioner().isSuitableForVgpu(vgpu, gddramShare)
						|| !pgpu.getBwProvisioner().isSuitableForVgpu(vgpu, bwShare)) {
					return false;
				}
				List<Pe> pgpuPes = pgpu.getPeList();
				int freePes = CollectionUtils.countMatches(pgpuPes, new Predicate() {
					@Override
					public boolean evaluate(Object arg) {
						Pe pe = (Pe) arg;
						if (pe.getPeProvisioner().getTotalAllocatedMips() == 0) {
							return true;
						}
						return false;
					}
				});
				if (freePes < mipsShare.size()) {
					return false;
				}
				// PEs in the pgpu are sorted according to their
				// available mips
				Collections.sort(pgpuPes, Collections.reverseOrder(new Comparator<Pe>() {
					public int compare(Pe pe1, Pe pe2) {
						return Double.compare(pe1.getPeProvisioner().getAvailableMips(),
								pe2.getPeProvisioner().getAvailableMips());
					}
				}));
				for (int i = 0; i < mipsShare.size(); i++) {
					if (mipsShare.get(i) > pgpuPes.get(i).getPeProvisioner().getAvailableMips()) {
						return false;
					}
				}
				if (getPgpuVgpuMap().get(pgpu).isEmpty()
						|| getPgpuVgpuMap().get(pgpu).get(0).getType() == vgpu.getType()) {
					return true;
				}
				return false;
			}
		});

		// if there is no candidate,
		Pgpu selectedPgpu = getPgpuSelectionPolicy().selectPgpu(this, candidates);
		if (selectedPgpu == null) {
			return false;
		}
		// if one pgpu is found suitable,
		// allocate gddram
		selectedPgpu.getGddramProvisioner().allocateGddramForVgpu(vgpu, gddramShare);
		// allocated gddram bandwidth
		selectedPgpu.getBwProvisioner().allocateBwForVgpu(vgpu, bwShare);
		// and finally, select pes
		List<Pe> selectedPgpuPes = (List<Pe>) CollectionUtils.select(selectedPgpu.getPeList(), new Predicate() {
			@Override
			public boolean evaluate(Object arg) {
				Pe pe = (Pe) arg;
				if (pe.getPeProvisioner().getTotalAllocatedMips() == 0) {
					return true;
				}
				return false;
			}
		});
		List<Pe> selectedPes = new ArrayList<Pe>();
		for (int i = 0; i < mipsShare.size(); i++) {
			Pe pe = selectedPgpuPes.get(i);
			pe.getPeProvisioner().allocateMipsForVm(vm, mipsShare.get(i));
			selectedPes.add(pe);
		}
		getPgpuVgpuMap().get(selectedPgpu).add(vgpu);
		getVgpuPeMap().put(vgpu, selectedPes);
		getMipsMap().put(vgpu, mipsShare);
		vgpu.setCurrentAllocatedMips(mipsShare);
		return true;
	}

	@Override
	public void deallocatePgpuForVgpu(Vgpu vgpu) {
		Pgpu pgpu = getPgpuForVgpu(vgpu);
		pgpu.getGddramProvisioner().deallocateGddramForVgpu(vgpu);
		pgpu.getBwProvisioner().deallocateBwForVgpu(vgpu);
		getPgpuVgpuMap().get(pgpu).remove(vgpu);
		for (Pe pe : getVgpuPeMap().get(vgpu)) {
			pe.getPeProvisioner().deallocateMipsForVm(vgpu.getVm());
		}
		getVgpuPeMap().remove(vgpu);
		getMipsMap().remove(vgpu);
		vgpu.setCurrentAllocatedMips(null);
	}

}
