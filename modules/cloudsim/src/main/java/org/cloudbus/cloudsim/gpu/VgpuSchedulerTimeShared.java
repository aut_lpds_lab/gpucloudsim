package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.gpu.selection.PgpuSelectionPolicy;
import org.cloudbus.cloudsim.gpu.util.GridVgpuUtil;

/**
 * VgpuSchedulerTimeShared is a vgpu allocation policy that allocates one or
 * more gpu PEs from a video card to a vgpu, and allows sharing of gpu PEs by
 * multiple vgpus. This scheduler does not support over-subscription.
 * 
 * @author Ahmad Siavashi
 */
public class VgpuSchedulerTimeShared extends VgpuScheduler {

	/**
	 * Instantiates a new vgpu time-shared scheduler.
	 * 
	 * @param pgpulist
	 *            the list of pgpus of the video card where the VgpuScheduler is
	 *            associated to.
	 */
	public VgpuSchedulerTimeShared(int videoCardType, List<Pgpu> pgpuList, PgpuSelectionPolicy pgpuSelectionPolicy) {
		super(videoCardType, pgpuList, pgpuSelectionPolicy);
	}

	/**
	 * Checks whether the vgpu type is supported by this video card type or not.
	 * 
	 * @param vgpu
	 *            the vgpu
	 * @return $true if the video card supports the vgpu type.
	 */
	protected boolean isVideoCardSuitableForVgpu(Vgpu vgpu) {
		if (!GridVgpuUtil.isVideoCardSuitable(getVideoCardType(), vgpu.getType())) {
			return false;
		}
		for (Entry<Pgpu, List<Vgpu>> entry : getPgpuVgpuMap().entrySet()) {
			if (GridVgpuUtil.isPgpuSuitable(entry, vgpu)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isSuitable(final Vgpu vgpu) {
		if (!isVideoCardSuitableForVgpu(vgpu)) {
			return false;
		}
		final List<Double> mipsShare = vgpu.getCurrentRequestedMips();
		final int gddramShare = vgpu.getCurrentRequestedGddram();
		final long bwShare = vgpu.getCurrentRequestedBw();
		@SuppressWarnings("unchecked")
		List<Pgpu> candidates = (List<Pgpu>) CollectionUtils.select(getPgpuList(), new Predicate() {
			@Override
			public boolean evaluate(Object arg) {
				Pgpu pgpu = (Pgpu) arg;
				if (!pgpu.getGddramProvisioner().isSuitableForVgpu(vgpu, gddramShare)
						|| !pgpu.getBwProvisioner().isSuitableForVgpu(vgpu, bwShare)) {
					return false;
				}
				if (pgpu.getPeList().size() < mipsShare.size()) {
					return false;
				}
				List<Pe> pgpuPes = pgpu.getPeList();
				// PEs in the pgpu are sorted according to their
				// available mips
				Collections.sort(pgpuPes, Collections.reverseOrder(new Comparator<Pe>() {
					public int compare(Pe pe1, Pe pe2) {
						return Double.compare(pe1.getPeProvisioner().getAvailableMips(),
								pe2.getPeProvisioner().getAvailableMips());
					}
				}));

				for (int i = 0; i < mipsShare.size(); i++) {
					if (mipsShare.get(i) > pgpuPes.get(i).getPeProvisioner().getAvailableMips()) {
						return false;
					}
				}
				return true;
			}
		});
		// No two Vgpu PEs are mapped to one Pgpu PE, hence there must
		// exists one such possible mapping at least
		// if no suitable pgpu found, exit
		if (candidates.isEmpty()) {
			return false;
		}
		return true;
	}

	@Override
	public boolean allocatePgpuForVgpu(final Vgpu vgpu, final List<Double> mipsShare, final int gddramShare,
			final long bwShare) {
		if (!isVideoCardSuitableForVgpu(vgpu)) {
			return false;
		}
		// TODO A worst-fit strategy is taken now, other strategies can be
		// taken as well
		// in order to consider process variation
		// ... mipsShare is sorted first in descending order
		Collections.sort(mipsShare, Collections.reverseOrder());

		// This scheduler does not allow over-subscription
		@SuppressWarnings("unchecked")
		List<Pgpu> candidates = (List<Pgpu>) CollectionUtils.select(getPgpuList(), new Predicate() {
			@Override
			public boolean evaluate(Object arg) {
				Pgpu pgpu = (Pgpu) arg;
				if (!pgpu.getGddramProvisioner().isSuitableForVgpu(vgpu, gddramShare)
						|| !pgpu.getBwProvisioner().isSuitableForVgpu(vgpu, bwShare)) {
					return false;
				}
				if (pgpu.getPeList().size() < mipsShare.size()) {
					return false;
				}
				List<Pe> pgpuPes = pgpu.getPeList();
				// PEs in the pgpu are sorted according to their
				// available mips
				Collections.sort(pgpuPes, Collections.reverseOrder(new Comparator<Pe>() {
					public int compare(Pe pe1, Pe pe2) {
						return Double.compare(pe1.getPeProvisioner().getAvailableMips(),
								pe2.getPeProvisioner().getAvailableMips());
					}
				}));

				for (int i = 0; i < mipsShare.size(); i++) {
					if (mipsShare.get(i) > pgpuPes.get(i).getPeProvisioner().getAvailableMips()) {
						return false;
					}
				}
				if (getPgpuVgpuMap().get(pgpu).isEmpty()
						|| getPgpuVgpuMap().get(pgpu).get(0).getType() == vgpu.getType()) {
					return true;
				}
				return false;
			}
		});
		// No two Vgpu PEs are mapped to one Pgpu PE, hence there must
		// exists one such possible mapping at least
		// if no suitable pgpu found, exit
		Pgpu selectedPgpu = getPgpuSelectionPolicy().selectPgpu(this, candidates);
		if (selectedPgpu == null) {
			return false;
		}
		selectedPgpu.getGddramProvisioner().allocateGddramForVgpu(vgpu, gddramShare);
		selectedPgpu.getBwProvisioner().allocateBwForVgpu(vgpu, bwShare);
		List<Pe> selectedPgpuPes = selectedPgpu.getPeList();
		List<Pe> selectedPes = new ArrayList<Pe>();
		for (int i = 0; i < mipsShare.size(); i++) {
			Pe pe = selectedPgpuPes.get(i);
			pe.getPeProvisioner().allocateMipsForVm(vgpu.getVm(), mipsShare.get(i));
			selectedPes.add(pe);
		}
		getPgpuVgpuMap().get(selectedPgpu).add(vgpu);
		getVgpuPeMap().put(vgpu, selectedPes);
		getMipsMap().put(vgpu, mipsShare);
		vgpu.setCurrentAllocatedMips(mipsShare);
		return true;

	}

	@Override
	public void deallocatePgpuForVgpu(Vgpu vgpu) {
		Pgpu pgpu = getPgpuForVgpu(vgpu);
		pgpu.getGddramProvisioner().deallocateGddramForVgpu(vgpu);
		pgpu.getBwProvisioner().deallocateBwForVgpu(vgpu);
		getPgpuVgpuMap().get(pgpu).remove(vgpu);
		for (Pe pe : getVgpuPeMap().get(vgpu)) {
			pe.getPeProvisioner().deallocateMipsForVm(vgpu.getVm());
		}
		getVgpuPeMap().remove(vgpu);
		getMipsMap().remove(vgpu);
		vgpu.setCurrentAllocatedMips(null);
	}

}
