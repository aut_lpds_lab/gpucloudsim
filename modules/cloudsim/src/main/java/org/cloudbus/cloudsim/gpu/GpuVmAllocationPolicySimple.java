package org.cloudbus.cloudsim.gpu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;

/**
 * {@link GpuVmAllocationPolicySimple} extends {@link VmAllocationPolicy} and
 * implements first-fit algorithm for VM placement.
 * 
 * @author Ahmad Siavashi
 *
 */
public class GpuVmAllocationPolicySimple extends VmAllocationPolicy {

	/**
	 * The map between each VM and its allocated host. The map key is a VM UID and
	 * the value is the allocated host for that VM.
	 */
	private Map<String, Host> vmTable;

	/**
	 * @param list
	 */
	public GpuVmAllocationPolicySimple(List<? extends Host> list) {
		super(list);
		setVmTable(new HashMap<String, Host>());
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		if (!getVmTable().containsKey(vm.getUid())) {
			for (Host host : getHostList()) {
				boolean result = allocateHostForVm(vm, host);
				if (result) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		if (!getVmTable().containsKey(vm.getUid())) {
			boolean result = host.vmCreate(vm);
			if (result) {
				getVmTable().put(vm.getUid(), host);
				Log.formatLine("%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId(),
						CloudSim.clock());
				Log.printLine("{'clock': " + CloudSim.clock() + ", 'event': 'vm allocation',  'vm': " + vm.getId()
						+ ", 'host': " + host.getId() + "}");
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deallocateHostForVm(Vm vm) {
		Host host = getVmTable().remove(vm.getUid());
		if (host != null) {
			Log.printLine("{'clock': " + CloudSim.clock() + ", 'event': 'vm deallocation',  'vm': " + vm.getId()
					+ ", 'host': " + host.getId() + "}");
			host.vmDestroy(vm);
		}
	}

	@Override
	public Host getHost(Vm vm) {
		return getVmTable().get(vm.getUid());
	}

	@Override
	public Host getHost(int vmId, int userId) {
		return getVmTable().get(Vm.getUid(userId, vmId));
	}

	/**
	 * @return the vmTable
	 */
	protected Map<String, Host> getVmTable() {
		return vmTable;
	}

	/**
	 * @param vmTable
	 *            the vmTable to set
	 */
	protected void setVmTable(Map<String, Host> vmTable) {
		this.vmTable = vmTable;
	}

}
