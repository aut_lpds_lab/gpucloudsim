/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

/**
 * 
 * Methods & constants that are related to {@link VideoCard VideoCards} types and configurations.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class VideoCardTags {

	// Constants

	public final static int VIDEO_CARD_CUSTOM = -1;
	public final static int NVIDIA_K1_CARD = 0;
	public final static int NVIDIA_K2_CARD = 1;

	public final static int NVIDIA_KEPLER_SMX_CUDA_CORES = 192;

	/** 15.75 GB/s */
	public final static int PCI_E_3_X16_BW = (int) (15.75 * 1025);

	// NVIDIA GRID K1 Spec

	/** 130 Watts */
	public final static int NVIDIA_K1_CARD_POWER = 130;
	/** 4 GPUs */
	public final static int NVIDIA_K1_CARD_GPUS = 4;
	/** 4 GBs/GPU */
	public final static int NVIDIA_K1_CARD_GPU_MEM = 4096;
	/** 1 SMX / GPU */
	public final static int NVIDIA_K1_CARD_GPU_PES = 1;
	/** 850 MHz */
	public final static double NVIDIA_K1_CARD_PE_MIPS = getGpuPeMipsFromFrequency(NVIDIA_K1_CARD, 850);
	/** 4 */
	public final static int NVIDIA_K1_CARD_NUM_BUS = 4;
	/** 4 x 28.5/s */
	public final static long NVIDIA_K1_CARD_BW_PER_BUS = (long) 28.5 * 1024;

	// NVIDIA GRID K2 Spec

	/** 225 Watts */
	public final static int NVIDIA_K2_CARD_POWER = 225;
	/** 2 GPUs */
	public final static int NVIDIA_K2_CARD_GPUS = 2;
	/** 4 GBs/GPU */
	public final static int NVIDIA_K2_CARD_GPU_MEM = 4096;
	/** 1 SMX / GPU */
	public final static int NVIDIA_K2_CARD_GPU_PES = 8;
	/** 750 MHz */
	public final static double NVIDIA_K2_CARD_PE_MIPS = getGpuPeMipsFromFrequency(NVIDIA_K2_CARD, 745);
	/** 2 */
	public final static int NVIDIA_K2_CARD_NUM_BUS = 2;
	/** 2 x 160.0 GB/s */
	public final static long NVIDIA_K2_CARD_BW_PER_BUS = 160 * 1024;

	public static double getGpuPeFrequencyFromMips(int type, double mips) {
		double frequency = mips;
		switch (type) {
		case NVIDIA_K1_CARD:
			frequency /= NVIDIA_KEPLER_SMX_CUDA_CORES * 2;
			break;
		case NVIDIA_K2_CARD:
			frequency /= NVIDIA_KEPLER_SMX_CUDA_CORES * 2;
			break;
		default:
			break;
		}
		return frequency;
	}

	public static double getGpuPeMipsFromFrequency(int type, double frequency) {
		double mips = frequency;
		switch (type) {
		case NVIDIA_K1_CARD:
			mips *= NVIDIA_KEPLER_SMX_CUDA_CORES * 2;
			break;
		case NVIDIA_K2_CARD:
			mips *= NVIDIA_KEPLER_SMX_CUDA_CORES * 2;
			break;
		default:
			break;
		}
		return mips;
	}

	/**
	 * Converts video card type to a representative string.
	 *  
	 * @param videoCardType
	 * @return a representative string of video card type
	 */
	public static String getVideoCardTypeString(int videoCardType) {
		switch (videoCardType) {
		case NVIDIA_K1_CARD:
			return "NVIDIA GRID K1";
		case NVIDIA_K2_CARD:
			return "NVIDIA GRID K2";
		default:
			return "Custom";
		}
	}

	/**
	 * Singleton class (i.e. cannot be initialized)
	 */
	private VideoCardTags() {
	}

}
