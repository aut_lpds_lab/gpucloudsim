package org.cloudbus.cloudsim.gpu.allocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.gpu.Vgpu;
import org.cloudbus.cloudsim.gpu.VideoCard;

/**
 * {@link VideoCardAllocationPolicyBreadthFirst} extends
 * {@link VideoCardAllocationPolicy} to provision video cards to vgpus on a
 * host. Host video cards are sorted in ascending order from the least loaded
 * video card to the most loaded one. Then, they are traversed one by one until
 * the newly arrived vgpu is allocated.
 * 
 * @author Ahmad Siavashi
 *
 */
public class VideoCardAllocationPolicyBreadthFirst extends VideoCardAllocationPolicySimple {

	private Map<VideoCard, List<Vgpu>> videoCardVgpuMap;

	/**
	 * Instantiates a new breadth-first allocation policy for video cards. Host
	 * video cards are sorted in ascending order from the least loaded video card to
	 * the most loaded one. Then, they are traversed one by one until the newly
	 * arrived vgpu is allocated.
	 * 
	 * @param videoCards
	 *            the video cards associated with the host.
	 */
	public VideoCardAllocationPolicyBreadthFirst(List<? extends VideoCard> videoCards) {
		super(videoCards);
		setVideoCardVgpuMap(new HashMap<>());
		for (VideoCard videoCard : getVideoCards()) {
			getVideoCardVgpuMap().put(videoCard, new ArrayList<>());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cloudbus.cloudsim.gpu.allocation.VideoCardAllocationPolicy#allocate(org.
	 * cloudbus.cloudsim.gpu.Vgpu, int)
	 */
	@Override
	public boolean allocate(Vgpu vgpu, int PCIeBw) {
		Collections.sort(getVideoCards(), new Comparator<VideoCard>() {
			@Override
			public int compare(VideoCard videoCard1, VideoCard videoCard2) {
				return Integer.compare(getVideoCardVgpuMap().get(videoCard1).size(),
						getVideoCardVgpuMap().get(videoCard2).size());
			}
		});
		boolean result = super.allocate(vgpu, PCIeBw);
		if (result) {
			VideoCard videoCard = getVgpuVideoCardMap().get(vgpu);
			getVideoCardVgpuMap().get(videoCard).add(vgpu);
		}
		return result;
	}

	@Override
	public boolean deallocate(Vgpu vgpu) {
		VideoCard videoCard = getVgpuVideoCardMap().get(vgpu);
		getVideoCardVgpuMap().get(videoCard).remove(vgpu);
		return super.deallocate(vgpu);
	}

	/**
	 * @return the videoCardVgpuMap
	 */
	public Map<VideoCard, List<Vgpu>> getVideoCardVgpuMap() {
		return videoCardVgpuMap;
	}

	/**
	 * @param videoCardVgpuMap
	 *            the videoCardVgpuMap to set
	 */
	protected void setVideoCardVgpuMap(Map<VideoCard, List<Vgpu>> videoCardVgpuMap) {
		this.videoCardVgpuMap = videoCardVgpuMap;
	}

}
