/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.PredicateType;
import org.cloudbus.cloudsim.gpu.core.GpuCloudSimTags;

/**
 * {@link GpuDatacenter} extends {@link Datacenter} to support
 * {@link GpuCloudlet}s as well as the memory transfer between CPU and GPU.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class GpuDatacenter extends Datacenter {

	/**
	 * Keeps the time in which the next
	 * {@link GpuCloudSimTags#GPU_VM_DATACENTER_EVENT} is going to occur. It helps
	 * removing redundant events that may get scheduled for the same time as only
	 * one is enough to occur.
	 */
	private double nextScheduledIntervalGpuVmDatacenterEventTime;

	/**
	 * See {@link Datacenter#Datacenter}
	 */
	public GpuDatacenter(String name, DatacenterCharacteristics characteristics, VmAllocationPolicy vmAllocationPolicy,
			List<Storage> storageList, double schedulingInterval) throws Exception {
		super(name, characteristics, vmAllocationPolicy, storageList, schedulingInterval);
		this.nextScheduledIntervalGpuVmDatacenterEventTime = CloudSim.getMinTimeBetweenEvents();
	}

	@Override
	protected void updateCloudletProcessing() {
		// if some time passed since last processing
		// R: for term is to allow loop at simulation start. Otherwise, one
		// initial
		// simulation step is skipped and schedulers are not properly
		// initialized
		if (CloudSim.clock() < 0.111 || CloudSim.clock() >= getLastProcessTime() + CloudSim.getMinTimeBetweenEvents()) {
			List<? extends Host> list = getVmAllocationPolicy().getHostList();
			double smallerTime = Double.MAX_VALUE;
			// for each host...
			for (int i = 0; i < list.size(); i++) {
				Host host = list.get(i);
				// inform VMs to update processing
				double time = host.updateVmsProcessing(CloudSim.clock());
				// what time do we expect that the next cloudlet will finish?
				if (time < smallerTime) {
					smallerTime = time;
				}
			}
			// gurantees a minimal interval before scheduling the event
			if (smallerTime - CloudSim.clock() < CloudSim.getMinTimeBetweenEvents()) {
				smallerTime += CloudSim.getMinTimeBetweenEvents();
			}
			if (smallerTime != Double.MAX_VALUE && smallerTime < nextScheduledIntervalGpuVmDatacenterEventTime) {
				schedule(getId(), smallerTime - CloudSim.clock(), GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);
			}
			setLastProcessTime(CloudSim.clock());
		}
	}

	@Override
	public void processEvent(SimEvent ev) {
		if (CloudSim.clock() == nextScheduledIntervalGpuVmDatacenterEventTime) {
			nextScheduledIntervalGpuVmDatacenterEventTime += getSchedulingInterval();
			CloudSim.cancelAll(getId(), new PredicateType(GpuCloudSimTags.GPU_VM_DATACENTER_EVENT));
			schedule(getId(), getSchedulingInterval(), GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);
		}
		super.processEvent(ev);
	}

	@Override
	protected void processOtherEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case GpuCloudSimTags.GPU_TASK_SUBMIT:
			processTaskSubmit(ev);
			break;
		case GpuCloudSimTags.GPU_VM_DATACENTER_EVENT:
			updateCloudletProcessing();
			checkCloudletCompletion();
			updateMemoryTransfers();
			checkMemoryTransfersCompletion();
			checkTaskCompletion();
			break;
		default:
			super.processOtherEvent(ev);
			break;
		}
	}

	protected void updateMemoryTransfers() {
		List<? extends Host> list = getVmAllocationPolicy().getHostList();
		double smallerTime = Double.MAX_VALUE;
		for (int i = 0; i < list.size(); i++) {
			GpuHost host = (GpuHost) list.get(i);
			if (host.getVideoCardAllocationPolicy() != null) {
				double time = host.updateVgpusMemoryTransfer(CloudSim.clock());
				if (time > 0.0 && time < smallerTime) {
					time = time < CloudSim.getMinTimeBetweenEvents() ? CloudSim.getMinTimeBetweenEvents() : time;
					smallerTime = time;
				}
			}
		}
		if (smallerTime != Double.MAX_VALUE
				&& (smallerTime + CloudSim.clock()) < nextScheduledIntervalGpuVmDatacenterEventTime) {
			send(getId(), smallerTime, GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);
		}
	}

	protected void checkMemoryTransfersCompletion() {
		List<? extends Host> list = getVmAllocationPolicy().getHostList();
		for (int i = 0; i < list.size(); i++) {
			GpuHost host = (GpuHost) list.get(i);
			for (Vm vm : host.getVmList()) {
				GpuVm gpuVm = (GpuVm) vm;
				Vgpu vgpu = gpuVm.getVgpu();
				if (vgpu != null) {
					while (vgpu.getGpuTaskScheduler().hasFinishedMemoryTransfers()) {
						ResGpuTask rcl = vgpu.getGpuTaskScheduler().getNextFinishedMemoryTransfer();
						if (rcl.getTaskStatus() == GpuTask.MEMORY_TRANSFER_HOST_TO_DEVICE) {
							double estimatedFinishTime = vgpu.getGpuTaskScheduler().taskSubmit(rcl);
							if (estimatedFinishTime > 0.0 && !Double.isInfinite(estimatedFinishTime)
									&& (estimatedFinishTime
											+ CloudSim.clock()) < nextScheduledIntervalGpuVmDatacenterEventTime) {
								send(getId(), estimatedFinishTime, GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);
							}
						}
					}
				}
			}
		}
	}

	protected void checkTaskCompletion() {
		List<? extends Host> list = getVmAllocationPolicy().getHostList();
		for (int i = 0; i < list.size(); i++) {
			GpuHost host = (GpuHost) list.get(i);
			for (Vm vm : host.getVmList()) {
				GpuVm gpuVm = (GpuVm) vm;
				Vgpu vgpu = gpuVm.getVgpu();
				if (vgpu != null) {
					while (vgpu.getGpuTaskScheduler().hasFinishedTasks()) {
						ResGpuTask rcl = vgpu.getGpuTaskScheduler().getNextFinishedTask();
						sendNow(rcl.getGpuTask().getCloudlet().getUserId(), CloudSimTags.CLOUDLET_RETURN,
								rcl.getGpuTask().getCloudlet());
					}
				}
			}
		}
	}

	@Override
	protected void checkCloudletCompletion() {
		List<? extends Host> list = getVmAllocationPolicy().getHostList();
		for (int i = 0; i < list.size(); i++) {
			Host host = list.get(i);
			for (Vm vm : host.getVmList()) {
				while (vm.getCloudletScheduler().isFinishedCloudlets()) {
					GpuCloudlet cl = (GpuCloudlet) vm.getCloudletScheduler().getNextFinishedCloudlet();
					GpuTask gt = cl.getGpuTask();
					if (gt != null) {
						sendNow(getId(), GpuCloudSimTags.GPU_TASK_SUBMIT, gt);
					} else {
						sendNow(cl.getUserId(), CloudSimTags.CLOUDLET_RETURN, cl);
					}
				}
			}
		}
	}

	@Override
	protected void processVmCreate(SimEvent ev, boolean ack) {
		Vm vm = (Vm) ev.getData();
		Log.printLine(CloudSim.clock() + ": Trying to Create VM #" + vm.getId() + " in " + getName());

		boolean result = getVmAllocationPolicy().allocateHostForVm(vm);

		if (ack) {
			int[] data = new int[3];
			data[0] = getId();
			data[1] = vm.getId();

			if (result) {
				data[2] = CloudSimTags.TRUE;
			} else {
				data[2] = CloudSimTags.FALSE;
			}
			send(vm.getUserId(), CloudSim.getMinTimeBetweenEvents(), CloudSimTags.VM_CREATE_ACK, data);
		}

		if (result) {
			getVmList().add(vm);
			GpuVm gpuVm = (GpuVm) vm;
			Vgpu vgpu = gpuVm.getVgpu();

			if (vm.isBeingInstantiated()) {
				vm.setBeingInstantiated(false);
				if (vgpu != null && vgpu.isBeingInstantiated()) {
					vgpu.setBeingInstantiated(false);
				}
			}

			vm.updateVmProcessing(CloudSim.clock(),
					getVmAllocationPolicy().getHost(vm).getVmScheduler().getAllocatedMipsForVm(vm));

			GpuHost gpuHost = (GpuHost) getVmAllocationPolicy().getHost(vm);
			if (vgpu != null) {
				vgpu.updateTaskProcessing(CloudSim.clock(), gpuHost.getVideoCardAllocationPolicy().getVgpuVideoCardMap()
						.get(vgpu).getVgpuScheduler().getAllocatedMipsForVgpu(vgpu));
			}
		}

	}

	protected void processTaskSubmit(SimEvent ev) {
		updateCloudletProcessing();

		try {
			// gets the task object
			GpuTask gt = (GpuTask) ev.getData();

			// checks whether this task has finished or not
			if (gt.isFinished()) {
				String name = CloudSim.getEntityName(gt.getCloudlet().getUserId());
				Log.printConcatLine(getName(), ": Warning - Task #", gt.getTaskId(), " of Cloudlet #",
						gt.getCloudlet().getCloudletId(), " owned by ", name, " is already completed/finished.");
				Log.printLine("Therefore, it is not being executed again");
				Log.printLine();

				sendNow(gt.getCloudlet().getUserId(), CloudSimTags.CLOUDLET_RETURN, gt.getCloudlet());

				return;
			}

			// process this task to this CloudResource
			gt.setResourceParameter(getId(), getCharacteristics().getCostPerSecond(),
					getCharacteristics().getCostPerBw());

			int userId = gt.getCloudlet().getUserId();
			int vmId = gt.getCloudlet().getVmId();

			GpuHost host = (GpuHost) getVmAllocationPolicy().getHost(vmId, userId);
			GpuVm vm = (GpuVm) host.getVm(vmId, userId);

			GpuTaskScheduler scheduler = vm.getVgpu().getGpuTaskScheduler();

			scheduler.taskSubmit(gt);

			sendNow(getId(), GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);

		} catch (ClassCastException c) {
			Log.printLine(getName() + ".processTaskSubmit(): " + "ClassCastException error.");
			c.printStackTrace();
		} catch (Exception e) {
			Log.printLine(getName() + ".processTaskSubmit(): " + "Exception error.");
			e.printStackTrace();
			System.exit(-1);
		}

		checkCloudletCompletion();
		checkTaskCompletion();
	}

	@Override
	protected void processCloudletSubmit(SimEvent ev, boolean ack) {
		updateCloudletProcessing();

		try {
			// gets the Cloudlet object
			Cloudlet cl = (Cloudlet) ev.getData();

			// checks whether this Cloudlet has finished or not
			if (cl.isFinished()) {
				String name = CloudSim.getEntityName(cl.getUserId());
				Log.printConcatLine(getName(), ": Warning - Cloudlet #", cl.getCloudletId(), " owned by ", name,
						" is already completed/finished.");
				Log.printLine("Therefore, it is not being executed again");
				Log.printLine();

				// NOTE: If a Cloudlet has finished, then it won't be processed.
				// So, if ack is required, this method sends back a result.
				// If ack is not required, this method don't send back a result.
				// Hence, this might cause CloudSim to be hanged since waiting
				// for this Cloudlet back.
				if (ack) {
					int[] data = new int[3];
					data[0] = getId();
					data[1] = cl.getCloudletId();
					data[2] = CloudSimTags.FALSE;

					// unique tag = operation tag
					int tag = CloudSimTags.CLOUDLET_SUBMIT_ACK;
					sendNow(cl.getUserId(), tag, data);
				}

				sendNow(cl.getUserId(), CloudSimTags.CLOUDLET_RETURN, cl);

				return;
			}

			// process this Cloudlet to this CloudResource
			cl.setResourceParameter(getId(), getCharacteristics().getCostPerSecond(),
					getCharacteristics().getCostPerBw());

			int userId = cl.getUserId();
			int vmId = cl.getVmId();

			// time to transfer the files
			double fileTransferTime = predictFileTransferTime(cl.getRequiredFiles());

			Host host = getVmAllocationPolicy().getHost(vmId, userId);
			Vm vm = host.getVm(vmId, userId);
			CloudletScheduler scheduler = vm.getCloudletScheduler();
			double estimatedFinishTime = scheduler.cloudletSubmit(cl, fileTransferTime);

			// if this cloudlet is in the exec queue
			if (estimatedFinishTime > 0.0 && !Double.isInfinite(estimatedFinishTime)
					&& (estimatedFinishTime + CloudSim.clock()) < nextScheduledIntervalGpuVmDatacenterEventTime) {
				estimatedFinishTime += fileTransferTime;
				// This line has been modified in comparison to the method it
				// overrides
				send(getId(), estimatedFinishTime, GpuCloudSimTags.GPU_VM_DATACENTER_EVENT);
			}

			if (ack) {
				int[] data = new int[3];
				data[0] = getId();
				data[1] = cl.getCloudletId();
				data[2] = CloudSimTags.TRUE;

				// unique tag = operation tag
				int tag = CloudSimTags.CLOUDLET_SUBMIT_ACK;
				sendNow(cl.getUserId(), tag, data);
			}
		} catch (Exception e) {
			Log.printLine(getName() + ".processCloudletSubmit(): " + "Exception error.");
			e.printStackTrace();
			System.exit(1);
		}

		checkCloudletCompletion();
		checkTaskCompletion();
	}

}
