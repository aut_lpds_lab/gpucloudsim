
## GPUCloudSim: an extension of CloudSim for modeling and simulation of GPUs in cloud data centers

In order to satisfy graphical and computational requirements of end-users, today cloud providers offer GPU-enabled services. It is known that due to the complexity of GPU devices, conventional virtualization techniques are not directly applicable. Hence, various virtualization methods such as API remoting, full, para and hardware-assisted virtualization techniques are adopted to share a GPU among multiple VMs. To ease-up conducting experimental studies on GPU-enabled cloud computing environments, we provide an extension to CloudSim simulator. Our extension includes models and provisioning and scheduling policies to enable the modeling and simulation of GPUs in data centers.  

-----------------------------------------
If you used the extension, please consider citing the following paper, 
> A. Siavashi and M. Momtazpour, “GPUCloudSim: an extension of CloudSim
> for modeling and simulation of GPUs in cloud data centers,” The
> Journal of Supercomputing, Oct. 2018.
-----------------------------------------
As the extension has not been tested for all conditions, you may find errors in runtime. In case of any problem, we would be grateful if you consider reporting the issue. Currently, we are adding new features to the extension.

### Main features

  * Support for modeling and simulation of large scale GPU-enabled Cloud platforms
  * Support for modeling and simulation of energy-aware GPU-equipped data centers
  * Support for modeling and simulation of multi-video card servers
  * Support for modeling and simulation of multi-GPU video cards
  * Support for user-defined GPU provisioning policies
  * Support for modeling and simulation of GPU-enabled VMs
  * Support for modeling and simulation of virtualization-incurred performance overhead
  * Support for user-defined GPU scheduling policies
  * Support for modeling and simulation of GPU applications
  * Support for modeling and simulation of interference among co-running GPU applications
  * Support for user-defined GPU application scheduling policies

### Download

This package contains CloudSim 4.0 with the latest version of the GPU extension.

### Disclaimer
This code is provided as is, and no guarantee is given that this code will preform in the desired way.

